package com.gitdab.conduit.yarnmd

sealed abstract case class MappingVersion(tag: String)

object MappingVersion {

  private[yarnmd] def apply(tag: String): MappingVersion = new MappingVersion(tag) {}

}
