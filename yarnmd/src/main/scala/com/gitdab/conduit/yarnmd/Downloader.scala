package com.gitdab.conduit.yarnmd

import java.net.URI

import org.typelevel.jawn._
import org.typelevel.jawn.ast.{JArray, JValue}

import scala.util.Try

object Downloader {

  val apiURL = "https://api.github.com/repos/FabricMC/yarn"
  val ghZipURL = "https://github.com/FabricMC/yarn/archive"
  val userAgent = "Mozilla/5.0 yarnmd"

  def versions: Try[List[MappingVersion]] = {
    val uri = new URI(apiURL).resolve("branches")
    val r = requests.get(uri.toString, headers = Map("User-Agent" -> userAgent))
    Parser
      .parseFromString[JValue](r.text())
      .map(j => {
        val JArray(vs) = j
        val childList = vs.toList.map { c =>
          val name = c.get("name").asString
          MappingVersion(name)
        }
        childList
      })
  }

  def latestVersion: Try[MappingVersion] = {
    val repoInfo = new URI(apiURL)
    val r = requests.get(repoInfo.toString, headers = Map("User-Agent" -> userAgent))
    Parser
      .parseFromString[JValue](r.text())
      .map(j => {
        MappingVersion(j.get("default_branch").asString)
      })
  }

  def zipURI(mv: MappingVersion): URI = new URI(ghZipURL).resolve(mv.tag)

  def zipData(mv: MappingVersion): Vector[Byte] = {
    val uri = zipURI(mv)
    val r = requests.get(uri.toString, headers = Map("User-Agent" -> userAgent))
    r.contents.toVector
  }

}
