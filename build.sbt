import sbtcrossproject.CrossPlugin.autoImport.{crossProject, CrossType}

scalaVersion in ThisBuild := "2.11.12"

val sharedSettings = Seq(
  name         := "yarn-mapping-downloader",
  organization := "com.gitdab.conduit",
  version      := "0.1.0",
  scalaVersion := "2.11.12",
  scalacOptions ++= Seq(
    "-unchecked",
    "-deprecation",
    "-feature"
  ),
  libraryDependencies ++= Seq(
    "com.lihaoyi" %% "requests"      % "0.1.4",
    "org.typelevel" %% "jawn-ast"    % "0.14.0",
    "org.typelevel" %% "jawn-parser" % "0.14.0"
  )
)

lazy val yarnmd =
  crossProject( /* JSPlatform, */ JVMPlatform /* , NativePlatform */ )
    .crossType(CrossType.Pure)
    .settings(sharedSettings)
//    .jsSettings(crossScalaVersions := Seq("2.11.12", "2.12.7"))
    .jvmSettings(crossScalaVersions := Seq("2.11.12", "2.12.7"))
//    .nativeSettings(crossScalaVersions := Seq("2.11.12"))

// lazy val yarnmdJS = yarnmd.js
lazy val yarnmdJVM = yarnmd.jvm
// lazy val yarnmdNative = yarnmd.native
